import React from 'react';
import ReactDOM from 'react-dom';
import Navigation from './Components/Navigation/Navigation';
import Footer from './Components/Footer/Footer';
import '../scss/app.scss';

class App extends React.Component {
    
    render() {
        return (
            <div>
                <Navigation />
                <Footer />
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));