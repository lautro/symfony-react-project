import React from 'react';
import {BrowserRouter, NavLink, Route, Switch} from 'react-router-dom';
import './Navigation.scss'
 
import Home from '../../Pages/Home/Home';
import Contact from '../../Pages/Contact/Contact';
import Login from '../../Pages/Login/Login';


class Navigation extends React.Component {
 
    render() {
        return (
            <BrowserRouter className="header">
                <div className="navbar navbar-expand-md bg-dark">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item" style={{margin: '10px'}}>
                            <NavLink className="btn bg-light nav-link text-dark" to="/">Home</NavLink>
                        </li>
                        <li className="nav-item" style={{margin: '10px'}}>
                            <NavLink className="btn bg-light nav-link text-dark" to="/contact">Contact</NavLink>
                        </li>
                    </ul>
                    <ul className="navbar-nav">
                        <li className="nav-item" style={{margin: '10px'}}>
                            <NavLink className="btn bg-light nav-link text-dark" to="/login">Login</NavLink>
                        </li>
                    </ul>
                </div>
                <div className="container">
                    <Switch>
                            <Route path="/login" component={Login}/>
                            <Route path="/contact" component={Contact}/>
                            <Route path="/" component={Home}/>
                    </Switch>
                </div>
            </BrowserRouter>
        );    
    }
}

export default Navigation;