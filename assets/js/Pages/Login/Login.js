import React, { Component } from 'react';
import {PostData} from '../../services/PostData';
import {Redirect} from 'react-router-dom';
import './Login.scss'
class Login extends Component {

    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',
            redirect: false
        }
        this.login = this.login.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    login(){
        if(this.state.username && this.state.password){
            PostData('login', this.state).then ((result) => {
                let responseJSON = result;
                if(responseJSON = result){
                    sessionStorage.setItem('userData', responseJSON);
                    this.setState({redirect: true});
                    console.log("home page");
                }
            });
        } else {
            console.log("login error")
        }
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value});
        console.log(this.state);
    }

    render() {

        if(this.state.redirect){
            return (<Redirect to={'/'} />)
        }

        return (
            <div className="form-group login">
                <h2>Login Page</h2>
                <input className="form-control u-p-label" type="text" name="username" placeholder="username" onChange={this.onChange} />
                <input className="form-control u-p-label" type="password" name="password" placeholder="password" onChange={this.onChange}  />
                <input className="btn btn-success submit-label" type="submit" name="Login" onClick={this.login} />
            </div>
        );
    }
}

export default Login;