import React from 'react';
import {Redirect} from 'react-router-dom';


class Contact extends React.Component 
{

  constructor(props){
    super(props);
    this.state={
        redirect: false
    }
  }

  componentWillMount(){
    if(sessionStorage.getItem("userData")){
      console.log("Call User Feed");
    } else {
      this.setState({redirect: true});
    }
  }

  render() {

    if(this.state.redirect){
      return (<Redirect to={'/login'} />)
    }

    return (
        <div>
          <p>Welcome to the contact-page!</p>
        </div>
    );
  }
}

export default Contact;